<?php 
include "config2.php";
session_start();

if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION);
    header("Location: login.php");
}

$sql = "SELECT * FROM users";


$result = $conn->query($sql);


?>

<!DOCTYPE html>
<html>
<head>
    <title>View Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>users</h2>
<table class="table">
    <thead>
        <tr>
        <th>ID</th>
        <th>FullName</th>
        <th>FirstName</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody> 
        <?php
            if ($result->num_rows > 0) {
                //output data of each row
                while ($row = $result->fetch_assoc()) {
        ?>

                    <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['fullname']; ?></td>
                    <td><?php echo $row['username']; ?></td>
                    <td><?php echo $row['email']; ?></td>
                    <td><a class="btn btn-info" href="proses_ubah.php?id=<?php echo $row['id']; ?>">Edit</a>&nbsp;<a class="btn btn-danger" href="proses_hapus.php?id=<?php echo $row['id']; ?>">Delete</a></td>
                    </tr>   
                    
        <?php       }
            }
        ?>
                
    </tbody>
</table>
<button><a href="register.php" class="ml-2">Create new account</a></button>
<button><a href="index.php?logout=true">Logout</a></button>
    </div>

</body>
</html>