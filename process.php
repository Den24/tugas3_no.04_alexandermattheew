<?php
require_once('config.php');
?>
<?php

if(isset($_POST)){

	$fullname = $_POST['fullname'];
	$username = $_POST['username'];
	$email    = $_POST['email'];
	$password = $_POST['password'];

        password_hash($password, PASSWORD_DEFAULT);
		$sql = "INSERT INTO users (fullname, username, email, password ) VALUES(?,?,?,?)";
		$stmtinsert = $db->prepare($sql);
		$result = $stmtinsert->execute([$fullname, $username, $email, $password]);
		if($result){
			echo 'Successfully saved.';
		}else{
			echo 'There were erros while saving the data.';
		}
}else{
	echo 'No data';
}